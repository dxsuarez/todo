//
//  Item.swift
//  ToDo
//
//  Created by Diego Suárez on 9/5/18.
//  Copyright © 2018 Diego Suárez. All rights reserved.
//

import Foundation

struct Item {
    let title:String
    let location:String
    let description:String
}
