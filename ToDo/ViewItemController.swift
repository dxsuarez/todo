//
//  ViewItemController.swift
//  ToDo
//
//  Created by Diego Suárez on 15/5/18.
//  Copyright © 2018 Diego Suárez. All rights reserved.
//

import UIKit

class ViewItemController: UIViewController {

    var itemInfo : (itemManager : ItemManager, index : Int)?
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let item = itemInfo?.itemManager.toDoItems[(itemInfo?.index)!]

        titleLabel.text = item?.title
        locationLabel.text = item?.location
        descriptionLabel.text = item?.description
    }
    
    @IBAction func checkButtonPressed(_ sender: Any) {
        itemInfo?.itemManager.checkItem(index: (itemInfo?.index)!)
        
        navigationController?.popViewController(animated: true)
        
    }
}
