//
//  AddItemViewController.swift
//  ToDo
//
//  Created by Diego Suárez on 9/5/18.
//  Copyright © 2018 Diego Suárez. All rights reserved.
//

import UIKit

class AddItemViewController: UIViewController {

    var itemManager:ItemManager?
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var locationTextFiel: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        
        let itemTitle = titleTextField.text ?? ""
        let itemLocation = locationTextFiel.text ?? ""
        let itemDescription = descriptionTextField.text ?? ""
        
        let item = Item(
            title: itemTitle,
            location: itemLocation,
            description: itemDescription
        )
        
        if(item.title == "") {
            shoWAlert(title: "Error", message: "Title is required")
        }
        
        itemManager?.toDoItems += [item]
        
        navigationController?.popViewController(animated: true)
    }
    
    func shoWAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler:nil)
        
        alert.addAction(okAction)
        
        present(alert, animated:true, completion: nil)
    }
}
